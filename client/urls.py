from django.urls import path 
from . import views 
from django.contrib.auth import views as auth_views


urlpatterns = [
path('acceuil',views.acceuil, name='acceuil'),
path('',views.index, name='index'),
path('ajouter',views.ajouter, name='ajouter'),
path('afficher',views.afficher, name='afficher'),
path('chercher',views.chercher, name='chercher'),
path('delete/<pseudo>', views.delete, name='delete'),
path('modifier/<pseudo>', views.modifier, name='modifier'),
path('ajouterHeb',views.ajouterHeb, name='ajouterHeb'),
path('afficherHeb',views.afficherHeb, name='afficherHeb'),
path('chercherHeb',views.chercherHeb, name='chercherHeb'),
path('ajouterHebergement',views.ajouterHebergement, name='ajouterHebergement'),
path('afficherHebergement',views.afficherHebergement, name='afficherHebergement'),
path('ajouterFacture/<pseudo>',views.ajouterFacture, name='ajouterFacture'),
path('connexion', views.connexion, name='connexion'),
path('deconnexion', views.deconnexion, name='deconnexion'),
path('demande',views.demande, name='demande'),
path('urgent',views.urgent, name='urgent'),
path('domaine',views.domaine, name='domaine'),
path('afficherDomaine',views.afficherDomaine, name='afficherDomaine'),
path('domaineExp',views.domaineExp, name='domaineExp'),
path('statistique',views.statistique, name='statistique'),
path('sendMail',views.sendMail, name='sendMail'),

]
