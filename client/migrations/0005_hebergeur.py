# Generated by Django 2.0.2 on 2018-03-25 00:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0004_auto_20180319_1846'),
    ]

    operations = [
        migrations.CreateModel(
            name='hebergeur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50)),
                ('contact', models.CharField(max_length=50)),
                ('adresse', models.CharField(max_length=60)),
                ('email', models.EmailField(max_length=50)),
            ],
        ),
    ]
