# Generated by Django 2.0.2 on 2018-03-29 20:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0011_hebergement_my_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hebergement',
            name='my_file',
        ),
    ]
