# Generated by Django 2.0.5 on 2018-05-29 00:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0025_auto_20180518_0319'),
    ]

    operations = [
        migrations.CreateModel(
            name='nomDommaine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dommaine', models.CharField(max_length=50)),
                ('hebergeur', models.EmailField(max_length=50)),
                ('prix', models.FloatField()),
            ],
        ),
    ]
