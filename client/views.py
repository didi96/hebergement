from django.shortcuts import render
from client import models
from client import  forms
from .models import client ,hebergeur ,hebergement ,ancienClient , DemandeForm ,alertes ,nomDomaine

from django.http import HttpResponse
from django.views.generic import View

from client.utils import render_to_pdf #created in step 4
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib.admin.views.decorators import staff_member_required
from datetime import date
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings


def acceuil(request):
  form=forms.DemandeForm(request.POST or None)
  msg = ''


  if form.is_valid():
    comments = models.DemandeForm()
    comments.nom = form.cleaned_data['nom']
    comments.email = form.cleaned_data['email']
    comments.comment = form.cleaned_data['comment']
    comments.save()
    msg= "Votre message a ete envoye , veillez se presenter dans notre entreprise pour ...."

  context={
        'formregister':form,
         'msg':msg
      
    }

  

  return render(request,'client/home.html' ,context)


@staff_member_required

def index(request):
	

	return render(request,'client/index.html')



@staff_member_required
def ajouter(request):

  form_data=forms.ClientForm(request.POST or None)
  msg=''
  
  if form_data.is_valid():
       contacts=models.client()
       contacts.pseudo=form_data.cleaned_data['pseudo']
       contacts.nom=form_data.cleaned_data['nom']
       contacts.prenom=form_data.cleaned_data['prenom']
       contacts.entreprise=form_data.cleaned_data['entreprise']
       contacts.email=form_data.cleaned_data['email']
       contacts.tel=form_data.cleaned_data['tel']
       contacts.whatsapp=form_data.cleaned_data['whatsapp']

       cl= models.client.objects.all()
       j=[]
       for cont in cl:

    
        j.append(cont.pseudo)
       pseudo=form_data.cleaned_data['pseudo']
       if pseudo not in j :
        contacts.save()
        msg='data is saved'
       else:
          msg= 'le pseudo existe deja , il faut changer le pseudo du client svp'
           


  context={
        'formregister':form_data,
         'msg':msg
      
    }
  return render(request,'client/clients/ajouter.html',context)



@staff_member_required
def afficher(request):
	client=models.client.objects.all()
	donnees={
        'client':client 
    }

    
	

	return render(request,'client/clients/afficher.html',donnees)



@staff_member_required
def chercher(request):


        msg="ce contact n'exist pas"
        form_data=forms.ClientCher(request.POST or None)
        form1={
                'formregister':form_data,
                'msg':msg
                }
        form ={'formregister':form_data}
        mes_contacts=models.client.objects.all()

        
        if form_data.is_valid():
                mes_contact=models.client.objects.all()

                pseudo=form_data.cleaned_data['pseudo']
                j=[]
                for cont in mes_contact:
                  j.append(cont.pseudo)
                
                if pseudo in j:
                        i = client.objects.get(pseudo=pseudo)

                        context ={
                        'client':i}
                        return render(request,'client/clients/cher_contact.html',context)
                else:
                        return render(request,'client/clients/chercher.html',form1)
        return render(request,'client/clients/chercher.html',form)


@staff_member_required
def delete(request,pseudo =None):
    cl = client.objects.get(pseudo=pseudo)
    ancien= models.ancienClient()
    ancien.pseudo = cl.pseudo
    ancien.nom = cl.nom
    ancien.prenom = cl.prenom
    ancien.entreprise = cl.entreprise
    ancien.email = cl.email
    ancien.tel = cl.tel
    ancien.whatsapp = cl.whatsapp
    ancien.save()
    cl.delete()
    ok ='retour vers affichage de contact'
    msg='le contact est supprimer'
    context = {
    'ok':ok,
    'msg':msg

    }
    
    return render(request,'client/clients/supprimer.html',context)

@staff_member_required
def modifier(request,pseudo):

    object = client.objects.get(pseudo=pseudo)


    form_data=forms.ClientUpdate(request.POST or None)
    msg=''
    ok='Annuler la mofication'
  
    if form_data.is_valid():
        
        object.nom=form_data.cleaned_data['nom']
        object.prenom=form_data.cleaned_data['prenom']
        object.entreprise=form_data.cleaned_data['entreprise']
        object.email=form_data.cleaned_data['email']
        object.tel=form_data.cleaned_data['tel']
        object.whatsapp=form_data.cleaned_data['whatsapp']

        object.save()
        msg='le contact est modifier avec succes'
        

    context={
        'formregister':form_data,
        'msg':msg,
        'ok': ok

         
       }

    return render(request,'client/clients/modifier.html',context)



@staff_member_required
def ajouterHeb(request):

  form_data=forms.HebergeurForm(request.POST or None)
  msg=''
  
  if form_data.is_valid():
       contacts=models.hebergeur()
       contacts.nom=form_data.cleaned_data['nom']
       contacts.email=form_data.cleaned_data['email']
       contacts.adresse=form_data.cleaned_data['adresse']
       contacts.penalites = form_data.cleaned_data['penalites']
       
       contacts.save()
       msg='data is saved'
       


  context={
        'formregister':form_data,
         'msg':msg
      
    }
  return render(request,'client/hebergeur/ajouter.html',context)

@staff_member_required
def afficherHeb(request):
  hebergeur=models.hebergeur.objects.all()
  donnees={
        'hebergeur':hebergeur 
    }

    
  

  return render(request,'client/hebergeur/afficher.html',donnees)

@staff_member_required
def chercherHeb(request):


        msg="ce contact n'exist pas"
        form_data=forms.HebergeurCher(request.POST or None)
        form1={
                'formregister':form_data,
                'msg':msg
                }
        form ={'formregister':form_data}
        mes_contacts=models.hebergeur.objects.all()

        
        if form_data.is_valid():
                mes_contact=models.hebergeur.objects.all()

                nom=form_data.cleaned_data['nom']
                j=[]
                for cont in mes_contact:
                  j.append(cont.nom)
                
                if nom in j:
                        i = hebergeur.objects.get(nom=nom)

                        context ={
                        'hebergeur':i}
                        return render(request,'client/hebergeur/cher_contact.html',context)
                else:
                        return render(request,'client/hebergeur/chercher.html',form1)
        return render(request,'client/hebergeur/chercher.html',form)


@staff_member_required
def ajouterHebergement(request):

  form_data=forms.HebergementForm(request.POST or None)
  msg=''
  datev = ''     # pour verfier que le date du debut est inferieur au date fin
  
  if form_data.is_valid():
       contacts=models.hebergement()
       contacts.clientH=form_data.cleaned_data['pseudoClient']
       contacts.hebergeurH=form_data.cleaned_data['nomHebergeur']
       contacts.nomDommaine=form_data.cleaned_data['nomDommaine']
       contacts.espaceStockage=form_data.cleaned_data['espaceStockage']
       contacts.bandePassante=form_data.cleaned_data['bandePassante']
       contacts.espaceBD=form_data.cleaned_data['espaceBD']
       contacts.responsable=form_data.cleaned_data['responsable']
       contacts.emailRes=form_data.cleaned_data['emailRes']
       contacts.dateDebut=form_data.cleaned_data['dateDebut']
       contacts.dateFin=form_data.cleaned_data['dateFin']
       contacts.prix=form_data.cleaned_data['prix']

       debut =  form_data.cleaned_data['dateDebut']
       fin = form_data.cleaned_data['dateFin']
       
       cl= models.client.objects.all()
       j=[]
       for cont in cl:

    
        j.append(cont.pseudo)
       nomClient=form_data.cleaned_data['pseudoClient']
       if (nomClient  in j) and (debut < fin) :
        contacts.save()
        msg='data is saved'
       else:
          if debut > fin :
            
            datev = "le date du debut doit etre inferieur au date du fin"
          else :
            msg= 'ce client n existe pas'
       



  context={
        'formregister':form_data,
         'msg':msg ,
         'datev':datev
      
    }
  return render(request,'client/hebergement/ajouter.html',context)


@staff_member_required
def afficherHebergement(request):

  
  hebergement=models.hebergement.objects.all()
  donnees={
        'hebergement':hebergement 
    }

    
  

  return render(request,'client/hebergement/afficher.html',donnees)



@staff_member_required
def ajouterFacture(request ,pseudo):
  
  object = hebergement.objects.get(id=pseudo)
  donnees ={
        'hebergement': object
         }

  pdf = render_to_pdf('client/pdf.html',donnees)
  
  if pdf:

    response = HttpResponse(pdf, content_type='client/pdf')
    filename = "Invoice_%s.pdf" %("12341231")
    content = "inline; filename='%s'" %(filename)
    download = request.GET.get("download")
    if download:
        content = "attachment; filename='%s'" %(filename)
    response['Content-Disposition'] = content
    return response

  return HttpResponse("Not found")
  
def connexion(request):
        error = False

    
        user =''
        form=forms.ConnexionForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)  # Nous vérifions si les données sont correctes
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)  # nous connectons l'utilisateur
            else: # sinon une erreur sera affichée
                error = True

    
        context={
        'formregister':form,
        'user':user ,
        'error':error
        }

      
    
        return render(request, 'client/connexion.html', context)

@staff_member_required
def deconnexion(request):
    logout(request)
    return redirect(reverse(acceuil))


@staff_member_required
def demande(request):
  demandes=models.DemandeForm.objects.all()
  donnees={
        'demandes':demandes 
    }

    
  

  return render(request,'client/form.html',donnees)




@staff_member_required
def register(request):

        registered = False
    

    
        
        # Get info from "both" forms
        # It appears as one form to the user on the .html page
        user_form = forms.UserForm(request.POST or None)
        profile_form = forms.UserProfileInfoForm(request.POST or None)

        # Check to see both forms are valid
        if user_form.is_valid() and profile_form.is_valid():

            # Save User Form to Database
            user = user_form.save()

            # Hash the password
            user.set_password(user.password)

            # Update with Hashed password
            user.save()

            # Now we deal with the extra info!

            # Can't commit yet because we still need to manipulate
            profile = profile_form.save(commit=False)

            # Set One to One relationship between
            # UserForm and UserProfileInfoForm
            profile.user = user

            # Check if they provided a profile picture
            if 'profile_pic' in request.FILES:
                print('found it')
                # If yes, then grab it from the POST form reply
                profile.profile_pic = request.FILES['profile_pic']

            # Now save model
            profile.save()

            # Registration Successful!
            registered = True

        else:
            # One of the forms was invalid if this else gets called.
            print(user_form.errors,profile_form.errors)

    
    # This is the render and context dictionary to feed
    # back to the registration.html file page.
        return render(request,'client/registration.html',
                          {'user_form':user_form,
                           'profile_form':profile_form,
                           'registered':registered})


# fonction pour capturer les hebergements expires
@staff_member_required
def urgent(request):
  urgents=models.hebergement.objects.all()

  
  liste =[]
  for x in urgents:
      
    
    dates={}
    if (date.today().year  > ((x.dateFin).year ) ) :
      
      dates["clientH"]=x.clientH
      dates["hebergeurH"]=x.hebergeurH
      dates["nomDommaine"]=x.nomDommaine
      dates["espaceStockage"]=x.espaceStockage
      dates["bandePassante"]=x.bandePassante
      dates["espaceBD"]=x.espaceBD
      dates["responsable"]=x.responsable
      dates["emailRes"]=x.emailRes
      dates["dateDebut"]=x.dateDebut
      dates["dateFin"]=x.dateFin
      dates["prix"]=x.prix
      liste.append(dates)
    else :
      if  (date.today().month  >= ((x.dateFin).month - 1 ) ) and (date.today().year  == ((x.dateFin).year ) ) :
        dates["clientH"]=x.clientH
        dates["hebergeurH"]=x.hebergeurH
        dates["nomDommaine"]=x.nomDommaine
        dates["espaceStockage"]=x.espaceStockage
        dates["bandePassante"]=x.bandePassante
        dates["espaceBD"]=x.espaceBD
        dates["responsable"]=x.responsable
        dates["emailRes"]=x.emailRes
        dates["dateDebut"]=x.dateDebut
        dates["dateFin"]=x.dateFin
        dates["prix"]=x.prix
        liste.append(dates)
    
  
  donnees={
        'donnees':liste 
    }

    
  

  return render(request,'client/hebergement/urgent.html',donnees)
@staff_member_required
def domaine(request):

  form_data=forms.DomaineForm(request.POST or None)
  msg=''
  
  if form_data.is_valid():
       domaines=models.nomDomaine()
       domaines.domaine=form_data.cleaned_data['domaine']
       domaines.hebergeur=form_data.cleaned_data['hebergeur']
       domaines.prix=form_data.cleaned_data['prix']
       domaines.dateDebut=form_data.cleaned_data['dateDebut']
       domaines.dateFin=form_data.cleaned_data['dateFin']

       
       domaines.save()
       msg='data is saved'
       


  context={
        'formregister':form_data,
         'msg':msg
      
    }
  return render(request,'client/domaine/ajouter.html',context)

@staff_member_required
def afficherDomaine(request):
  domaine=models.nomDomaine.objects.all()
  donnees={
        'domaine':domaine 
    }

    
  

  return render(request,'client/domaine/afficher.html',donnees)

@staff_member_required
def domaineExp(request):

  urgents=models.nomDomaine.objects.all()
  
  
  liste =[]
  for x in urgents:
      
    
    dates={}
    if (date.today().year  > ((x.dateFin).year ) ) :
      hb = hebergeur.objects.get(nom=x.hebergeur)
      
      dates["domaine"]=x.domaine
      dates["hebergeur"]=x.hebergeur
      dates["prix"]=x.prix
      dates["dateDebut"]=x.dateDebut
      dates["dateFin"]=x.dateFin
      dates["penalites"]=hb.penalites
     
      liste.append(dates)
    else :
      if  (date.today().month  >= ((x.dateFin).month - 1 ) ) and (date.today().year  == ((x.dateFin).year ) ) :
        hb = hebergeur.objects.get(nom=x.hebergeur)
        dates["domaine"]=x.domaine
        dates["hebergeur"]=x.hebergeur
        dates["prix"]=x.prix
        dates["dateDebut"]=x.dateDebut
        dates["dateFin"]=x.dateFin
        dates["penalites"]=hb.penalites
        liste.append(dates)
    
  
  donnees={
        'donnees':liste 
    }

    
  

  return render(request,'client/domaine/urgent.html',donnees)


@staff_member_required
def statistique(request):
  nbclient = client.objects.all().count()
  nbhebergeur = hebergeur.objects.all().count()
  nbdomaine = nomDomaine.objects.all().count()

  nbhebergement = hebergement.objects.all().count()

  domaines = models.nomDomaine.objects.all()
  prixDomaine = 0
  for domaine in domaines:
    prixDomaine = prixDomaine + domaine.prix
  
  hebergements = models.hebergement.objects.all()
  prixHebergement = 0
  for heb in hebergements:
    prixHebergement = prixHebergement + heb.prix
  
  prixSt = 100
  total =    prixHebergement - ( prixSt + prixDomaine )
  donnees={
        'nbclient':nbclient ,
        'nbhebergeur':nbhebergeur ,
        'nbdomaine':nbdomaine,
        'nbhebergement':nbhebergement ,
        'total': total ,
        'prixDomaine': prixDomaine ,
        'prixHebergement': prixHebergement ,
        'prixSt':prixSt 


    }
  return render(request,'client/statistique/statistique.html',donnees)


def sendMail(request):

  form_data=forms.sendMail(request.POST or None)
  msg=''
  if form_data.is_valid():

       
    from_email=form_data.cleaned_data['to']
    subject=form_data.cleaned_data['objet']
    message=form_data.cleaned_data['message']
    email_from = settings.EMAIL_HOST_USER
    if subject and message and from_email:
        try:
            send_mail(subject, message,email_from, [from_email])
        except BadHeaderError:
            msg = 'Invalid header found.'
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/contact/thanks/')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        msg = 'Make sure all fields are entered and valid.'
        return HttpResponse('Make sure all fields are entered and valid.')


  context={
        'formregister':form_data,
        'msg':msg
      
    }
       
  return render(request,'client/mail.html',context)


