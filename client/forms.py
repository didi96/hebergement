from django import forms
from django.core.exceptions import ValidationError
import re
from django.contrib.auth.models import User
from client.models import UserProfileInfo , hebergeur ,client





def validation(value):
		expression = r"^[2-4][0-9]([ .-]?[0-9]{2}){3}$"
		if re.search(expression, value) is None :
			raise ValidationError("ecrivez correctement le numero du telephone")
def valid(value):
		expression = r"^[a-zA-Z]+$"
		if re.search(expression, value) is None :
			raise ValidationError("le champ doit etre composee que des caracteres")
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username','email','password')


class UserProfileInfoForm(forms.ModelForm):
    class Meta():
        model = UserProfileInfo
        fields = ('profile_pic',)



class ClientForm(forms.Form):
	pseudo = forms.CharField(required=True,max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	nom = forms.CharField(required=True , max_length=20,validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	prenom = forms.CharField(required=True,max_length=20, validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	entreprise = forms.CharField(required=True,max_length=20, validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))

	email = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	tel = forms.CharField(required=True,max_length=8,validators=[validation], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	whatsapp = forms.CharField(required=True,max_length=8,validators=[validation], widget=forms.TextInput(attrs={'class': 'form-control'} ))


	
class ClientCher(forms.Form):
        pseudo = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'} ))
        
class ClientUpdate(forms.Form):
	nom = forms.CharField(required=True , max_length=20,validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	prenom = forms.CharField(required=True,max_length=20, validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	entreprise = forms.CharField(required=True,max_length=20, validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))

	email = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	tel = forms.CharField(required=True,max_length=8,validators=[validation], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	whatsapp = forms.CharField(required=True,max_length=8,validators=[validation], widget=forms.TextInput(attrs={'class': 'form-control'} ))


	
class HebergeurForm(forms.Form):
	"""docstring for HebergeurForm"""
	nom = forms.CharField(required=True , max_length=20,validators=[valid], widget=forms.TextInput(attrs={'class': 'form-control'} ))
	email = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	adresse = forms.CharField(required=True,max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	penalites = forms.CharField(label="penalites" ,widget=forms.Textarea(attrs={'class': 'form-control','placeholder':'Entrer votre demande ','rows':'10'} ))




class HebergeurCher(forms.Form):
        nom = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'} ))

class HebergementForm(forms.Form):
	pseudoClient = forms.ModelChoiceField(queryset=client.objects.all())
	nomHebergeur = forms.ModelChoiceField(queryset=hebergeur.objects.all())
	nomDommaine = forms.CharField(required=True,max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	espaceStockage = forms.CharField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	bandePassante= forms.CharField(required=True,max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	espaceBD= forms.CharField(required=True,max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	responsable= forms.CharField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	emailRes = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	dateDebut = forms.DateField(required=True)
	dateFin = forms.DateField(required=True)

	prix = forms.DecimalField(required=True )


class Facture(forms.Form):
        nomClient = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'} ))



class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30 ,widget=forms.TextInput(attrs={'class': 'form-control'} ))
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(attrs={'class': 'form-control'} ))


class DemandeForm(forms.Form):
	nom = forms.CharField(label="Entrer votre nom", max_length=40 ,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Nom'} ))
	email = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Email'} ))
	comment = forms.CharField(label="commentaire" ,widget=forms.Textarea(attrs={'class': 'form-control','placeholder':'Entrer votre demande ','rows':'10'} ))

class DomaineForm(forms.Form): 


		domaine = forms.CharField(required=True,max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'} ))
		hebergeur = forms.ModelChoiceField(queryset=hebergeur.objects.all())
		prix = forms.DecimalField(required=True )
		dateDebut = forms.DateField(required=True)
		dateFin = forms.DateField(required=True)




class sendMail(forms.Form):
	
	to = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Email'} ))
	objet =forms.CharField(label="objet", max_length=30 ,widget=forms.TextInput(attrs={'class': 'form-control'} ))
	message  = forms.CharField(label="message" ,widget=forms.Textarea(attrs={'class': 'form-control','placeholder':'Entrer votre message ','rows':'10'} ))