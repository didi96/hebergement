from django.db import models
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class UserProfileInfo(models.Model):

    # Create relationship (don't inherit from User!)
    user = models.OneToOneField(User,on_delete=models.DO_NOTHING)

    
    # pip install pillow to use this!
    # Optional: pip install pillow --global-option=”build_ext” --global-option=”--disable-jpeg”
    profile_pic = models.ImageField(upload_to='profile_pics',blank=True)

    def __str__(self):
        # Built-in attribute of django.contrib.auth.models.User !
        return self.user.username
class client(models.Model):
	"""docstring for contacts"""

	pseudo = models.CharField(max_length=50 ,primary_key=True)
	nom = models.CharField(max_length=50)
	prenom = models.CharField(max_length=50)
	entreprise = models.CharField(max_length=50)
	email = models.EmailField(max_length=50)
	tel = models.CharField(max_length=8)
	whatsapp = models.CharField(max_length=8)


	def __str__(self):
                return str(self.pseudo)
                
class hebergeur(models.Model):
	"""docstring for hebergeur"""
	nom = models.CharField(max_length=50)
	adresse = models.CharField(max_length=60)
	email = models.EmailField(max_length=50)
	penalites = models.CharField(max_length=200 ,default= "   ")

	def __str__(self):
                return str(self.nom)

	
fs = FileSystemStorage(location='/client/pdf')

class hebergement(models.Model):
	"""docstring for hebergement"""
	clientH = models.CharField(max_length=50 )
	hebergeurH = models.CharField(max_length=50 )
	nomDommaine = models.CharField(max_length=60)
	espaceStockage = models.CharField(max_length=50)
	bandePassante = models.CharField(max_length=50)
	espaceBD = models.CharField(max_length=50)
	responsable = models.CharField(max_length=50)
	emailRes = models.EmailField(max_length=50)
	dateDebut = models.DateField()
	dateFin = models.DateField()
	prix = models.FloatField()
	
	

class ancienClient(models.Model):
	"""docstring for contacts"""
	pseudo = models.CharField(max_length=50 ,primary_key=True)
	nom = models.CharField(max_length=50)
	prenom = models.CharField(max_length=50)
	entreprise = models.CharField(max_length=50)
	email = models.EmailField(max_length=50)
	tel = models.CharField(max_length=8)
	whatsapp = models.CharField(max_length=8)

class DemandeForm(models.Model):

    nom = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    comment = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

class alertes(models.Model):
	clientH = models.CharField(max_length=50 )
	hebergeurH = models.CharField(max_length=50 )
	nomDommaine = models.CharField(max_length=60)
	espaceStockage = models.CharField(max_length=50)
	bandePassante = models.CharField(max_length=50)
	espaceBD = models.CharField(max_length=50)
	responsable = models.CharField(max_length=50)
	emailRes = models.EmailField(max_length=50)
	dateDebut = models.DateField()
	dateFin = models.DateField()
	prix = models.FloatField()
	
	
class nomDomaine(models.Model):
    
    domaine = models.CharField(max_length=50)
    hebergeur = models.EmailField(max_length=50)
    prix = models.FloatField()
    dateDebut= models.DateField(default=timezone.now )
    dateFin=models.DateField(default=timezone.now )

	
	





