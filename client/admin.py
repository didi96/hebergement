from django.contrib import admin

# Register your models here.
from . models import client , hebergeur , hebergement , ancienClient ,DemandeForm ,UserProfileInfo , alertes ,nomDomaine
admin.site.register(client)
admin.site.register(hebergeur)
admin.site.register(hebergement)
admin.site.register(ancienClient)
admin.site.register(DemandeForm)
admin.site.register(UserProfileInfo)
admin.site.register(alertes)
admin.site.register(nomDomaine)




